#include "Register.h"

Register::Register() = default;

// overloaded Constructor
Register::Register(const string& path){
    loadData(path);
}

Register::Register(const Register& other)
{
    deleteAllRecords();
    Subordinates subordinates;
    map<string, string> tempMapOfSubordinates;
    for(auto _employee : other._employees){
        auto *myRecord = new Employee(*_employee);
        _employees.push_back(myRecord);
        _indexByName.insert(make_pair(myRecord->getName(), myRecord));
        insertRecordInSet(myRecord->getDepartment(),myRecord,_indexByDepartments);
        insertRecordInSet(myRecord->getDepartment(),myRecord,_indexByDepartments);
        tempMapOfSubordinates.insert(pair<string, string>(myRecord->getName(),myRecord->getBoss()));
        for(const string& workingday:myRecord->getWorkingDays()){
            insertRecordInSet(workingday,myRecord,_indexByWorkingDays);
        }
    }
    subordinates = findEmployees(tempMapOfSubordinates);
    insertSubordinates(subordinates);
}

// deconstructor
Register::~Register(){
    // destroy all pointers
    deleteAllRecords();
}

// operator overloading "=" to assign (copy) one object to another, with different pointers
Register& Register::operator= (const Register& rhv){
    if(this == &rhv){
        return *this;
    }
    Register temp(rhv);
    swap(temp, *this);
    return *this;
}

// public functions
void Register::deleteAllRecords(){
    for(Employee *employee:_employees){
        delete employee;
    }
    _employees.clear();
    _indexByName.clear();
    _indexByDepartments.clear();
    _indexBySubordinates.clear();
    _indexByWorkingDays.clear();
}

bool Register::isDataSetEmpty(){
    return _employees.empty();
}


void Register::loadData(const string& path){
    if(!isDataSetEmpty()){
        deleteAllRecords();
    }

    fstream fin;
    fin.open(path, ios::in);
    vector<string> row, workingDays;
    string line, word;

    Subordinates subordinates;
    map<string, string> tempMapOfSubordinates;

    while(getline(fin, line)) {
        stringstream s(line);

        row.clear();
        workingDays.clear();
    
        while (getline(s, word, '\t')) {
            if (row.size() >= 5) {
                workingDays.push_back(word);
            }
            row.push_back(word);
        }

        auto *myRecord = new Employee(row[0], stoi(row[1]), row[2], row[3], row[4], workingDays);

        _employees.push_back(myRecord);

        _indexByName.insert(make_pair(row[0], myRecord));

        insertRecordInSet(row[2], myRecord, _indexByDepartments);

        tempMapOfSubordinates.insert(pair<string, string>(row[0], row[4]));

        for (const string &workingday: workingDays) {
            insertRecordInSet(workingday, myRecord, _indexByWorkingDays);
        }
    }

    subordinates = findEmployees(tempMapOfSubordinates);
    insertSubordinates(subordinates);
}


void Register::printAllRecords() const{
    cout<<"------- All records -------"<<endl;
    EmplVector employees = getStorage();
    for(Employee *employee:employees){
        cout<<employee;
    }
}

void Register::printDataSetSize() const{
    cout<<"------- Database size: -------"<<endl;
    size_t dbSize = _employees.size();
    cout<<"Number of records: "<<dbSize<<" records"<<endl;
}

void Register::printRecordByName(string& name) const{
    cout<<"------- "<<"Chosen name: "<<name<<" -------"<<endl;
    auto it = _indexByName.find(name);
    if (it != _indexByName.end()){
        cout<<it->second;
    }else{
        cout << "No entry with the name "<<name<<" found. Please try again.";
    }
}

void Register::printRecordsByDepartment(string& department) const{
    cout<<"------- "<<"Department: "<<department<<" -------"<<endl;
    auto it = _indexByDepartments.find(department);
    if (it != _indexByDepartments.end()){
        for(Employee *employee:it->second){ //Loop through set
            cout<<employee;
        }
    }else{
        cout << "There are no records for department "<<department<<" found. Please try again.";
    }
}

void Register::printRecordsByBoss(string& boss) const{
    cout<<"------- "<<"Boss: "<<boss<<" -------"<<endl;
    cout<<"Subordinates: "<<endl;
    auto it = _indexBySubordinates.find(boss);
    if (it != _indexBySubordinates.end()){
        for(Employee *employee:it->second){ //Loop through set
            cout<<employee;
        }
    }else{
        cout << "There are no records for boss: "<<boss<<" found. Please try again.";
    }
}

void Register::printRecordsByWorkingDay(string& workDay) const{
    cout<<"------- "<<"Workday: "<<workDay<<" -------"<<endl;
    auto it = _indexByWorkingDays.find(workDay);
    if (it != _indexByWorkingDays.end()){
        for(Employee *employee:it->second){ //Loop through set
            cout<<employee;
        }
    }else{
        cout << "There are no records for Workday "<<workDay<<" found. Please try again.";
    }
}

void Register::printEmployeesInAgeRange(size_t& lowerLimit,size_t& upperLimit) const{
    cout<<"------- All records between age: "<<lowerLimit<<" and "<<upperLimit<<" -------"<<endl;
    EmplVector employees = getStorage();
    for(Employee *employee:employees){
        if(lowerLimit<=employee->getAge() && employee->getAge()<=upperLimit)
            cout<<employee;
    }
}

void Register::swap(Register& lhv, Register rhv) noexcept{
    std::swap(lhv._employees, rhv._employees);
    std::swap(lhv._indexByName, rhv._indexByName);
    std::swap(lhv._indexByDepartments, rhv._indexByDepartments);
    std::swap(lhv._indexBySubordinates, rhv._indexBySubordinates);
    std::swap(lhv._indexByWorkingDays, rhv._indexByWorkingDays);
}

const EmplVector& Register::getStorage() const{
    return _employees;
}


const IndexByName& Register::getIndexByName() const{
    return _indexByName;
}

void Register::insertRecordInSet(string mapKey,Employee *employee,IndexSetOfEmployees& indexSetofEmployees){
    auto it = indexSetofEmployees.find(mapKey);
    if (it != indexSetofEmployees.end()){
        it->second.insert(employee);
        set<Employee*> mySet = {employee};
        indexSetofEmployees.insert(make_pair(mapKey,mySet));
    }
}

void Register::insertSubordinates(Subordinates subordinates) {
    for (auto p: subordinates) {
        for (const string &subordinate: p.second) {
            auto it = _indexByName.find(subordinate);
            insertRecordInSet(const_cast<string &>(p.first), it->second, _indexBySubordinates);
        }
    }
}


set<string> Register::findAllReportingEmployees(const string& boss,map<string, set<string>> &managerToEmployeeMappings,map<string, set<string>> &result)
{
    if (result.find(boss) != result.end())
    {
        return result[boss];
    }

    set<string> managerEmployees = managerToEmployeeMappings[boss];

    for (const string& employee: managerToEmployeeMappings[boss])
    {
        set<string> employees = findAllReportingEmployees(employee,managerToEmployeeMappings, result);

        for (const string& c: employees){
            managerEmployees.insert(c);
        }
    }

    result[boss] = managerEmployees;
    return managerEmployees;
}


map<string, set<string>> Register::findEmployees(map<string, string> &employeeToManagerMappings)
{
    map<string, set<string>> managerToEmployeeMappings;

    for (auto& it: employeeToManagerMappings)
    {
        string employee = it.first;
        string boss = it.second;

        if (employee != boss) {
            managerToEmployeeMappings[boss].insert(employee);
        }
    }

    map<string, set<string>> result;

    for (auto p: employeeToManagerMappings)
    {
        findAllReportingEmployees(p.first, managerToEmployeeMappings, result);
    }

    return result;
}