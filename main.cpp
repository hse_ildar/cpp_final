#include "Register.h"

using namespace std;

int main(){

    // initialize
    Register employees = Register();

    string UI=
        "\n"
        "\n"
        "Employee database\n"
        "=================\n"
        "(L) Load a file\n"
        "(C) Clear the dataset\n"
        "(N) Print number of records in the dataset\n"
        "(P) Print all records\n"
        "(E) Print an employee by their name\n"
        "(D) Print all employees by their department\n"
        "(A) Print all employees with an age in a given range\n"
        "(S) Print all direct & indirect subordinates from an Employee as a whole.\n"
        "(W) Print all Employee, who work on a specified workday.\n"
        "(X) Exit\n"
        "Choose an action:\n";

    string input, line, isDataSetEmpty, filePath;
    size_t lowerAgeLimit, upperAgeLimit;
    cout<<UI;
    // loop, until "is 'X'", then return 0 and finish program
    while(true){ 

        cin >> input;
        if (input.length() != 1) {
            cout << "You've typed in not exactly 1 character."<<endl;
            cout << "please type exactly 1 characters to choose among the executable options."<<endl;
        } else {
            switch (toupper(input[0])) {
                case 'X':
                    return 0;
                case 'L':
                    cout << "Please specify the filepath: " << endl;
                    cin >> filePath;
                    if (!employees.isDataSetEmpty()) {
                        employees.deleteAllRecords();
                    }
                    employees.loadData(filePath);
                    if (employees.isDataSetEmpty()) {
                        cout<<"Something went wrong.. The data could not be loaded correctly." << endl;
                        break;}
                    break;
                case 'C':
                    if (employees.isDataSetEmpty()) {
                        break;
                    }
                    employees.deleteAllRecords();
                    cout << "Database successfully deleted! Please load new data." << endl;
                    break;
                case 'N':
                    if (employees.isDataSetEmpty()) {
                        break;
                    }
                    employees.printDataSetSize();
                    break;
                case 'P':
                    if (employees.isDataSetEmpty()) {
                        break;
                    }
                    employees.printAllRecords();
                    break;
                case 'E':
                    if (employees.isDataSetEmpty()) {
                        break;
                    }
                    cout << "Please type in the employees 'full name' e.g. 'Caius Mueller'" << endl;
                    getline(cin >> ws, line);
                    employees.printRecordByName(line);
                    break;
                case 'D':
                    if (employees.isDataSetEmpty()) {
                        break;
                    }
                    cout << "Please type in the department e.g. 'acc','it','crm','mngr','head'" << endl;
                    getline(cin >> ws, line);
                    employees.printRecordsByDepartment(line);
                    break;
                case 'A':
                    if (employees.isDataSetEmpty()) {
                        break;
                    }
                    cout << "Please insert lower age limit:" << endl;
                    cin >> lowerAgeLimit;
                    cout << "Please insert upper age limit:" << endl;
                    cin>>upperAgeLimit;
                    employees.printEmployeesInAgeRange(lowerAgeLimit,upperAgeLimit);
                    break;
                case 'S':
                    if (employees.isDataSetEmpty()) {
                        break;
                    }
                    cout << "Please type in the employees 'full name' in e.g. 'Asiyah Joseph'" << endl;
                    getline(cin >> ws, line);
                    employees.printRecordsByBoss(line);
                    break;
                case 'W':
                    if (employees.isDataSetEmpty()) {
                        break;
                    }
                    cout<<"Please type one of the following workdays: 'Mon','Tue','Wed','Thu','Fri','Sat','Sun'"<<endl;
                    getline(cin >> ws, line);
                    employees.printRecordsByWorkingDay(line);
                    break;

                default:
                    break;
            }
        }
        isDataSetEmpty = employees.isDataSetEmpty()?"\nAttention: Please load data (L) firstly, before choosing any other option.":"\nDataSet "+ filePath + " loaded.";
        cout << isDataSetEmpty;
        cout << UI;
    }

    return 0;
}