#ifndef FINAL_PROJECT_REGISTER_H
#define FINAL_PROJECT_REGISTER_H

#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <map>
#include <set>
#include <algorithm>

#include "Record.h"

using namespace std;

typedef Record Employee;
typedef vector<Employee*> EmplVector;
typedef map<string, Employee*> IndexByName;
typedef map<string, set<Employee*>> IndexSetOfEmployees;
typedef map<string, set<string>> Subordinates;

class Register {

public:

    // default Constructor
    Register();

    // overloaded Constructor
    explicit Register(const string& path);

    Register(const Register& other);

    // deconstructor
    ~Register();

    // overloading the "=" operator to assign (copy) one object to another with different pointers
    Register& operator= (const Register& rhv);

    void deleteAllRecords();

    // loading data
    void loadData(const string& path);

    // checking is empty
    bool isDataSetEmpty();

    void printAllRecords() const;
    void printDataSetSize() const;
    void printRecordByName(string& name) const;
    void printRecordsByDepartment(string& department) const;
    void printRecordsByBoss(string& boss) const;
    void printRecordsByWorkingDay(string& workDay) const;
    void printEmployeesInAgeRange(size_t& lowerLimit,size_t& upperLimit) const;

private:
    EmplVector _employees;
    IndexByName _indexByName;
    IndexSetOfEmployees _indexByDepartments;
    IndexSetOfEmployees _indexBySubordinates;
    IndexSetOfEmployees _indexByWorkingDays;

    static void swap(Register& lhv, Register rhv) noexcept;

    const EmplVector& getStorage() const;

    const IndexByName& getIndexByName() const;

    // helpers
    void insertRecordInSet(string mapKey,Employee *employee,IndexSetOfEmployees& indexSetofEmployees);
    void insertSubordinates(Subordinates subordinates);

    // report to a given manager and store the result in "result".
    set<string> findAllReportingEmployees(const string& boss,map<string, set<string>> &managerToEmployeeMappings,map<string, set<string>> &result);
    map<string, set<string>> findEmployees(map<string, string> &employeeToManagerMappings);
};


#endif