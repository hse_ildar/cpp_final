#ifndef FINAL_PROJECT_RECORD_H
#define FINAL_PROJECT_RECORD_H

#include <iostream>
#include <vector>
#include <string>

using namespace std;

typedef vector<string> vecWorkingDays;


class Record {

public:
    Record(const string &empName,const size_t &empAge,const string &empDepartment,const string &empPosition ,const string &empBoss, const vecWorkingDays &empWorkingDays);

    string getName() const;
    size_t getAge() const;
    string getDepartment() const;
    string getPosition() const;
    string getBoss() const;
    vecWorkingDays getWorkingDays() const;

private:
    string _empName;
    size_t _empAge;
    string _empDepartment;
    string _empPosition;
    string _empBoss;
    vecWorkingDays _empWorkingDays;

    friend ostream& operator<<(ostream& os, Record* record);
};


#endif